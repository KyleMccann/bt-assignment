import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Last edited by Kyle McCann on 07/06/2016.
 * Submission for BT Industrial Placement
 */
public class MainTest {

    @Test
    public void testEquals() throws Exception{
        String str= "Junit is working";

        //check for equality
        assertEquals("Junit is working", str);
    }


    @Test
    public void testMain() throws Exception {

    }


    @Test
    public void testReadFile() throws Exception {

    }

    @Test
    public void testSearch() throws Exception {

     String term = "gui";

        //Test ability to handle Packages with no known dependencies
        String expected = (term +  " -> ");
        String actual = Main.search(term);
        assertEquals("check if it handles null values",expected, actual);


        //Test Search Term
        Main.lhm.put(term,"awtui swingui");
        String expectedreturn = (term +" -> awtui swingui");
        String actualreturn = Main.search(term);
        assertEquals("check to find term",expectedreturn, actualreturn);


    }
}