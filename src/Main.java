/**
 * Last edited by Kyle McCann on 07/06/2016.
 * Submission for BT Industrial Placement
 */
import java.io.*;
import java.util.*;

public class Main {
    // Create a hash map
    public static LinkedHashMap lhm = new LinkedHashMap();
    public static void main(String[] args) throws Exception {

       // First argument in CLI is the fileName.
       readFile(args[0]);

        //Thereafter all further arguments are regarded as packages to be searched for
        for (int i =1; i < args.length; i++){ //go through each argument

            System.out.println(search(args[i])); //Search for current argument
        }


    }


    public static void readFile(String dependencyFile){
        TreeSet tree = new TreeSet(); //used for Storing dependencies in natural order
        BufferedReader reader = null;

        try {
            File file = new File(dependencyFile);
            reader = new BufferedReader(new FileReader(file));


            String line;

            while ((line = reader.readLine()) != null) {
                String[] values;

                values = line.split("->");
                String packageName = values[0].trim(); // This is the package name
                String dependencies =""; // the name of the dependency

                if (values.length>1){ //Handle packages without any dependencies
                    dependencies = values[1];

                    tree.clear();


                    StringTokenizer st = new StringTokenizer(dependencies, " ");
                    while(st.hasMoreTokens())
                       tree.add(st.nextToken());

                }
                String treeToString = tree.toString(); //Convert tree to string to allow for removal of []
                lhm.put(packageName, treeToString.substring(1, treeToString.length()-1 )); // Add Package Name and values. (Remove start and end [])

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


 public static String search(String term) {
String result ="";

     if (lhm.get(term) != null) {
         result = (term + " -> " + lhm.get(term));

     }
     if (lhm.get(term) == null){ // Handle Packages with no known dependancies
         result = (term + " -> ");
     }

     return result;
 }





}